## Leitfragen zur Blackbox-Testfallerstellung

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-leitfragen</span>


> **tl/dr;** _(ca. 90 min Bearbeitungszeit): Beispielfragen rund um Testgetriebene Entwicklung (_Testdriven Development_, TDD), Testfallerstellung aus Whitebox- und Blackbox-Systematik._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)

### Leitfragen Blackbox-Tests:

1) Was versteht man unter Blackbox-Tests? <button onclick="toggleAnswer('bb1')">Antwort</button>

  <span class="hidden-answer" id="bb1">
  Blackbox-Tests bestehen aus Testfällen, die gegen die Anforderungen erstellt werden. Unabhängig davon, ob der Code bekannt oder unbekannt ist, ob er bereits existiert oder noch nicht implementiert wurde, werden lediglich die in der Spezifikation beschriebenen Eingabe- und Ausgabewerte zur Erstellung der Testfälle herangezogen. siehe
  [Artikel zu  Blackbox-Testfällen](https://oer-informatik.de/blackboxtests)
  </span>

2) Welche Systematiken können zur Generierung von Blackbox-Testfällen genutzt werden?<button onclick="toggleAnswer('bb2')">Antwort</button>

  <span class="hidden-answer" id="bb2">
  Blackbox-Testfälle werden durch die beiden Systematiken _Grenzwertanalyse_ und _Äquivalenzklassenbildung_ gebildet.
  </span>

3) Was versteht man unter Äquivalenzklassenbildung? <button onclick="toggleAnswer('bb3')">Antwort</button>

  <span class="hidden-answer" id="bb3">
  Eine Äquivalenzklasse fasst ein Set von Eingabewerten zusammen, bei dem wir ein ähnliches Verhalten unseres getesteten Systems (_system under test_ - SUT) erwarten. Die Menge aller möglichen Eingabewerte für unser _SUT_ wird so in möglichst wenige Gruppen aufgeteilt.
  ![Äquivalenzklassen](https://oer-informatik.gitlab.io/qs/blackboxtest/images/grenzwert_rabatt.png)
  </span>

4) Was versteht man unter Grenzwert-Analyse? <button onclick="toggleAnswer('bb4')">Antwort</button>

  <span class="hidden-answer" id="bb4">
  Grenzwerte sind diejenigen Eingabewerte für unser _SUT_ (_System under Test)_), bei dem sich das Verhalten ändert. Häufig finden sich Grenzwerte an den Grenzen des Definitionsbereichs oder an den Grenzen von Äquivalenzklassen. Da hier besonders häufig Fehler passieren, wird im Rahmen der Grenzwertanalyse der Grenzwert selbst sowie die direkten Nachbarwerte getestet.
  ![Grenzwerte](https://oer-informatik.gitlab.io/qs/blackboxtest/images/grenzwert_rabatt.png)
  </span>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
