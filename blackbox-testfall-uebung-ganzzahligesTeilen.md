## Kleine Übungsaufgabe zu Blackbox-Testfallerstellung

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-ganzzahligesTeilen</span>


> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgaben zur Erstellung von Testfällen nach Blackboxsystematik für eine Methode, die Division mit Rest vornimmt (wie in der Grundschule)._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)


Testfallerstellung: Es soll eine einfache Funktion getestet werden, die ganzzahliges Teilen wie in der Grundschule durchführt und das Ergebnis als Zeichenkette ausgibt:

```python
def dividiere(divident:int, divisor:int)-> str:
    ganzzahl_ergebnis = divident // divisor
    rest_ergebnis = divident % divisor
    aufgabe = str(divident)+ " : " +str(divisor)+" = "
    ergebnis = str(ganzzahl_ergebnis) + " Rest " + str(rest_ergebnis)
    return aufgabe + ergebnis
```

Ausgabe: `10 : 3 = 3 Rest 1`

Wir kennen zwei Systematiken zur Blackbox-Testfallerstellung. Dokumentiere für jede Systematik zwei Testfälle für die obige Funktion. Nenne dabei neben der genutzten Systematik auch alle relevanten Werte, die für eine Testfalldokumentation wichtig sind! Bereite eine fachgerechte tabellarische Testfalldokumentation vor und trage die Testfälle darin ein mit allen Daten, die bereits bekannt sind (die anderen Zellen können vorbereitet, aber leer bleiben). Nenne auch die zugrunde gelegte Systematik. <button onclick="toggleAnswer('bba1')">Antwort</button>

  <div class="hidden-answer" id="bba1">

  |Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte<br/> (Parameter `divident`, `divisor`) | Erwartetes<br/>Resultat gemäß Spezifikation| Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
  --- | --- | --- |--- | --- | --- |--- | ---
  |1. | ÄQ: Ergebnis ist Ganzzahl | keine |`divident=8`<br/>`divisor=4`| 2 | - |2| ok|
  |2. | ÄK: Ergebnis ist Gleitkommazahl | keine |`divident=1`<br/>`divisor=10`| 0.1<br/> auf sechs signifikante Stellen genau | - | 0.1 | ok 	
  |3. | GW: Teilen durch 0 | keine |`divident=1`<br/>`divisor=0`| UI muss zur Korrektur der Werte auffordern | - | Exception | fail
  |4. | GW: Teilen mit 0 | keine |`divident=0`<br/>`divisor=1`| 0 | - | 0 | ok

  </div>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
