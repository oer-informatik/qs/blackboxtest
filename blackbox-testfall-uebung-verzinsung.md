## Übungsaufgabe zur Blackbox-Testfallerstellung für eine Verzinsungsfunktion

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-</span>


> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgabe zur Erstellung von Blackbox-Testfällen zu einer Funktion, die Guthaben/Soll eines Kontos verzinst._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)

Es sollen Blackbox-Testfälle erstellt werden für eine Methode zur Verzinsung eines Girokontos 

```java
double verzinse(double betrag, int jahr)
```

Die Methode erhält als Parameter den zu verzinsenden Betrag (in Euro) und die Verzinsungsdauer (in Jahren). Rückgabewert sind die resultierenden Zinsen (in Euro). Die Auftraggeberin übergibt als Anforderung folgende Liste:

- Guthaben bis zu (inkl.) einer Höhe von $3000€$ werden mit 0% verzinst.

- Guthaben oberhalb $3000€$ werden mit einem Strafzins von $-1%$ verzinst.

- Negative Guthaben von bis zu (inkl.) $-1500€$ werden mit dem Zinssatz $5%$ verzinst (geduldete Kontoüberziehungen).

-	Bei negative Guthaben unterhalb von $-1500€$ soll eine Ausnahme (Exception) geworfen werden.

-	Die Verzinsung erfolgt jährlich einmal, bei mehreren Jahren ergeben sich so Zinseszinsen.

a) Die Methode `double verzinse(double betrag, int jahr)` soll fachgerecht mit Testfällen nach Blackboxsystematik versehen werden. Bereite eine fachgerechte tabellarische Testfalldokumentation vor und trage darin vier Testfälle (zwei je Blackbox-Systematik) ein mit allen Daten, die bereits bekannt sind (die anderen Zellen können vorbereitet, aber leer bleiben).Nenne auch die zugrunde gelegte Systematik.

  <button onclick="toggleAnswer('bba1')">Antwort</button>

  <div class="hidden-answer" id="bba1">

  Allein über die Kombinatorik der beiden Parameter sind hier viele Testfälle möglich. Um effizient zu testen sollte jeweils der _Base-Case_ (die Standardanwendung) des einen Parameters mit den Grenzwerten des zweiten Kombiniert werden.

  |Typ|Name|Eingabewerte|Erwartetes<br/>Ergebnis(*)|Tats.<br/>Erg.(*)|Best-<br/>anden?(*)|
  |---|---|---|---|---|---|
  |GW1a|kein Guthaben|`verzinse(0.00, 1)`|0|
  |GW1b|kein Guthaben|`verzinse(0.01, 1)`|0|
  |GW1c|kein Guthaben|`verzinse(-0.01, 1)`|0.0005|
  |GW2a|geduldet, Untergrenze|`verzinse(-1500.00, 1)`|75|
  |GW2b|geduldet, Untergrenze|`verzinse(-1500.01, 1)`|`Exception`|
  |GW2c|geduldet, Untergrenze|`verzinse(-1499.99, 1)`||
  |GW3a|Strafzins, Untergrenze|`verzinse(3000.00, 1)`|0|
  |GW3b|Strafzins, Untergrenze|`verzinse(3000.01, 1)`|30|
  |GW3c|Strafzins, Untergrenze|`verzinse(2999.99, 1)`|0|
  |GW4|kein Zeitraum|`verzinse(500, 0)`|0|
  |ÄQ1|ein Jahr|`verzinse(500, 1)`|0|
  |ÄQ2|viele Jahre|`verzinse(500, 10)`|0|
  |ÄQ3|geduldet|`verzinse(-500, 5)`|-138,14|
  |ÄQ4|Strafzins|`verzinse(4000, 5)`|-196,04|
  |ÄQ5|Überzogen|`verzinse(-2000, 5)`|`Exception`|

  (*) Wird später ausgefüllt.

  </div>

b) Welche Spezifikationslücken fallen beim erstellen der Testfälle auf? Wie können diese gefüllt werden?


  <button onclick="toggleAnswer('bbb')">Antwort</button>

  <div class="hidden-answer" id="bbb">

  Es gibt keine Festlegung, was bei Eingabe von negativen Zeiten passieren soll. Ausserdem ist ungenau formuliert, was passiert, wenn der Wert im ersten Jahr oberhalb eines Grenzwerts liegt, in den Folgejahren aber unterhalb (z.B. Testfall `GW3b` oben: im ersten Jahr oberhalb der $3000$, dann mit $2970$ unterhalb).

  Was soll an den Grenzen des Definitionsbereichs passieren? Muss das Verhalten bei `MAX_DOUBLE` und `MIN_DOUBLE` bzw. wenn der Rückgabewert aus den Definitionsbereich geht getestet werden?
  
  </div>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
