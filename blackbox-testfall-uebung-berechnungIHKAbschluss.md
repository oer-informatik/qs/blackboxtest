## Blackboxtest- und Algorithmus-Übungsaufgabe "IHK-Abschlussnote"

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-berechnungIHKAbschluss</span>


> **tl/dr;** _(ca. 25 min Bearbeitungszeit): Übungsaufgaben zur Erstellung von Testfällen nach Blackboxsystematik für eine Methode, die die IHK-Abschlussnote berechnet. Es werden systematisch Äquivalenzklassen und Grenzwerte bestimmt und abschließend der Algorithmus mit Pseudocode formuliert._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)

Für eine Funktion zur Berechnung der IHK-Abschlussnote der IT-Berufe sollen Blackbox-Tests entwickelt werden. 

Die Funktion `abschlussnote(note[]: HashMap): int` soll die Note des Berufsabschlusses aus den Einzelnoten (Wertung von `0` bis `100` Punkte) berechnet. Die Einzelergebnisse werden in einer _Hashmap_ (einem _Dictionary_ nach Python-) `note` übergeben und können über die in der Tabelle unten genannten _Keys_ angesprochen werden (z.B. `note["T2P"] = 84` ).  Die _Values_ (die Punkte der Teilleistungen) sind jeweils vom Typ `int`.

Nach gleichem Muster werden die Gewichtungen in einer _Hashmap_ hinterlegt – diese ist global verfügbar und unveränderlich (z.B. `gewichtung["PJ"] = 25`).

Als Anforderungen ist gegeben:

-	Die Rückgabe der Funktion soll die gewichtete Durchschnittsnote sein (0 bis 100 Punkte), wenn die Prüfung bestanden wurde.

-	Die Funktion soll eine _Exception_ werfen, wenn die Berechnung aufgrund ungültiger Eingaben/Werte nicht möglich ist.

-	Die Rückgabe soll der Wert 0 sein, wenn die Prüfung nicht bestanden wurde.

-	Die Bestehensregeln sind folgende^[Siehe [Verordnung über die Berufsausbildung zum/zur Fachinformatiker*in, §16](https://www.gesetze-im-internet.de/fiausbv/__16.html)]:

    +	Im Gesamtergebnis (gewichteter Durchschnitt Teil 1 und Teil 2 zusammen) müssen mindestens 50 Punkte erreicht werden.

    +	Im Ergebnis von Teil 2 (gewichteter Durchschnitt von allem, bis auf die erste Prüfungsleistung) müssen mindestens 50 Punkte erreicht werden.

    +	In mindestens drei Prüfungsbereichen von Teil 2 müssen jeweils mindestens 50 Punkte erreicht werden.

    +	In keinem Prüfungsbereich von Teil 2 dürfen weniger als 30 Punkte erreicht werden.

Ein beispielhaftes Prüfungsergebnis besteht aus den folgenden Prüfungsleistungen:

||Prüfungsbestandteil|Key in Hashmap|Gewichtung|Beispiel Punkte|
|---|:---|---|---|---|
|Teil 1|Einrichten eines IT-gestützten Arbeitsplatzes	|T1|	20%|	        56| 
|Teil 2|Planen eines Softwareprodukts	|T2P|	10%|	        84| 
||Entwicklung und Umsetzung von Algorithmen	|T2A|	10%|	        60| 
||Wirtschaft und Sozialkunde|T2W|	10%|	        81| 
||Projektarbeit|PJ|25%|78| 
||Präsentation und Fachgespräch|FG|25%|85| 
||Gesamtergebnis:|||74|

Ein vereinfachter Funktionsaufruf kann für die Testfälle mit JSON-Notation erfolgen, für obiges Beispiel also:

```javascript
abschlussnote({"T1":56, "T2P":84, "T2A":60, "T2W":81, "PJ":78, "FG":85})
```

Die Funktion `abschlussnote(note[]: HashMap): int` soll fachgerecht mit Testfällen nach Blackboxsystematik versehen werden. 

1. Bereite eine fachgerechte tabellarische Testfalldokumentation vor und trage darin vier Testfälle (zwei je Blackbox-Systematik) ein mit allen Daten, die bereits bekannt sind (die anderen Zellen können vorbereitet, aber leer bleiben). Nenne auch die zugrunde gelegte Systematik.

<button onclick="toggleAnswer('bba2')">Antwort</button>

  <div class="hidden-answer" id="bba2">

  |Typ|Name|Eingabewerte|Erwartetes<br/>Ergebnis|Tats.<br/>Erg.(*)|Bestanden?(*)|
  |---|---|---|---|---|---|
  |GW|Knapp Bestanden|`abschlussnote({"T1":50, "T2P":50, "T2A":50, "T2W":50, "PJ":50, "FG":50})`|50|
  |GW|Beste Note|`abschlussnote({"T1":100, "T2P":100, "T2A":100, "T2W":100, "PJ":100, "FG":100})`|100|		
  |ÄQ|Bestanden|`abschlussnote({"T1":65, "T2P":65, "T2A":65, "T2W":65, "PJ":65, "FG":65})`|	65|		
  |ÄQ|Nicht Betstanden<br/>wg. sechs|`abschlussnote({"T1":65, "T2P":65, "T2A":25, "T2W":65, "PJ":65, "FG":65})`|0|		

  (*) Wird später ausgefüllt.

  </div>

2. Vervollständige die Testsuite, in dem Du Grenzwerttests für alle extremen Notenwerte ergänzt. Bilde darüber hinaus Äquivalenzklassen für jede Art, die Prüfung nicht zu bestehen. Ergänze auch ungültige Eingaben gemäß Spezifikation. Fasse dabei gleichartige tabellarische Testfalldokumentation zusammen und nenne jeweils die zugrunde gelegte Systematik.

  <button onclick="toggleAnswer('bba3')">Antwort</button>


  <div class="hidden-answer" id="bba3">

  Allein über die Kombinatorik sind hier viele Testfälle möglich. Um effizient zu testen, sollte jedoch jede Bestehens-Anforderung einmal erfüllt und einmal (isoliert) nicht erfüllt sein. Kombinationen aus unterschiedlichen Nicht-Bestehens-Kriterien sollten vermieden werden (jedenfalls so lange die einzelnen Kriterien noch nicht isoliert getestet wurden).

  |Typ|Name|Eingabewerte|Erwartetes<br/>Ergebnis|Tats.<br/>Erg.(*)|Best-<br/>anden?(*)|
  |---|---|---|---|---|---|
  |GW1|Schlechteste Note|`abschlussnote({"T1":0, "T2P":0, "T2A":0, "T2W":0, "PJ":0, "FG":0})`|0|
  |GW2|Alles knapp<br/>nicht bestanden|`abschlussnote({"T1":49, "T2P":49, "T2A":49, "T2W":49, "PJ":49, "FG":49})`|49|	
  |GW3|Alles knapp<br/>bestanden|`abschlussnote({"T1":50, "T2P":50, "T2A":50, "T2W":50, "PJ":50, "FG":50})`|50|
  |GW4|Beste Note|`abschlussnote({"T1":100, "T2P":100, "T2A":100, "T2W":100, "PJ":100, "FG":100})`|100|		
  |ÄQ1|Alles bestanden|`abschlussnote({"T1":65, "T2P":65, "T2A":65, "T2W":65, "PJ":65, "FG":65})`|	65|	
  |ÄQ2|Alles nicht<br/> bestanden|`abschlussnote({"T1":40, "T2P":40, "T2A":40, "T2W":40, "PJ":40, "FG":40})`|	0|		
  |ÄQ3|Nicht bestanden<br/>wg. sechs|`abschlussnote({"T1":85, "T2P":25, "T2A":85, "T2W":85, "PJ":85, "FG":85})`|0|
  |ÄQ4|Nicht bestanden<br/>Gesamtergebnis `<50`|`abschlussnote({"T1":51, "T2P":51, "T2A":51, "T2W":51, "PJ":51, "FG":32})`|0|			
  |ÄQ5|Nicht bestanden<br/>Teil 2 `<50`|`abschlussnote({"T1":90, "T2P":55, "T2A":55, "T2W":55, "PJ":55, "FG":35})`|0|	
  |ÄQ6|Nicht bestanden<br/>nicht mind 3xT2 `>=50`|`abschlussnote({"T1":90, "T2P":48, "T2A":48, "T2W":48, "PJ":55, "FG":55})`|0|
  |ÄQ7|Knapp bestanden<br/> trotz T2 5|`abschlussnote({"T1":85, "T2P":31, "T2A":53, "T2W":53, "PJ":53, "FG":53})`|57|
  |ÄQ8|Knapp bestanden<br/> trotz T1 0P|`abschlussnote({"T1":0, "T2P":65, "T2A":65, "T2W":65, "PJ":65, "FG":65})`|52|	
  |ÄQ9|Ungültig<br/>Einzelnote über 100P|`abschlussnote({"T1":65, "T2P":65, "T2A":65, "T2W":65, "PJ":65, "FG":101})`|`Exception`|	
  |ÄQ10|Ungültig<br/>Einzelnote kleiner 0|`abschlussnote({"T1":75, "T2P":65, "T2A":65, "T2W":65, "PJ":-1, "FG":65})`|`Exception`|
  |ÄQ11|Ungültig<br/>Einzelnote leer|`abschlussnote({"T1":85, "T2P":65, "T2A":65, "T2W":, "PJ":65, "FG":65})`|`Exception`|	
  |ÄQ12|Ungültig<br/>Einzelnote Gleitkomma|`abschlussnote({"T1":85, "T2P":65, "T2A":65.5, "T2W":, "PJ":65, "FG":65})`|`Exception`|
  |ÄQ13|Ungültig<br/>Einzelnote fehlt|`abschlussnote({"T1":85, "T2A":65.5, "T2W":, "PJ":65, "FG":65})`|`Exception`|
  |GW5|Ungültig<br/>leere Hashmap|`abschlussnote({})`|`Exception`|
  (*) Wird später ausgefüllt.

  </div>

3. Die Funktion, für die in der vorigen Aufgabe Blackboxtestfälle erstellt wurden, soll nun mit Pseudocode als Algorithmus entworfen werden. Setze die Anforderungen aus der vorigen Aufgabe zur Berechnung der Note und zu den Bestehensregeln um.

```java
abschlussnote(note[]: HashMap): int
```


### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
