## Leitfragen zu testgetriebener Entwicklung (TDD)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109287342438127241</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/tdd-leitfragen</span>

> **tl/dr;** _(ca. 90 min Bearbeitungszeit): Leitfragen zum Überprüfen des Verständnisses von Testgetriebene Entwicklung (_Testdriven Development_, TDD)._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Testgetriebene Entwicklung (mit pytest)](https://oer-informatik.de/python_einstieg_pytest)

### Leitfragen Testgetriebene Entwicklung:

1) Was versteht man unter Testgetriebener Entwicklung (TDD)? <button onclick="toggleAnswer('tdd1')">Antwort</button>

  <span class="hidden-answer" id="tdd1">
  Bei Testgetriebener Entwicklung wird zunächst ein Test erstellt, der einen Teil der Spezifikation überprüft und erst im zweiten Schritt die zugehörige Implementierung, die den Test bestehen lässt.
  </span>


2)  Welche drei Phasen gibt es in der Testgetriebenen Entwicklung, wie werden sie genannt, und welche Aufgaben hast Du als Entwicker*in in dieser Phase? <button onclick="toggleAnswer('tdd2')">Antwort</button>

  <span class="hidden-answer" id="tdd2">
  TDD umfass diese drei sich permanent wiederholenden Phasen:
    * Red: Erstelle einen Test, der einen kleinen (!) Bereich der Spezifikation umfasst. Dieser soll zunächst scheitern.
    * Green: Implementiere gerade so viel Code, damit der zuvor erstellte Test bestanden wird.
    * Refactor: Verbessere - wenn nötig - den Code des Tests und der Implementierung, ohne die Funktionalität zu verändern.
    ![Der TDD-Kreislauf: Red, Green, Refactor](https://oer-informatik.gitlab.io/python-basics/erste-schritte-mit-python/images/tdd.png)
  </span>


3) Testgetriebenen Entwicklung wurde v.a. im Umfeld eines bestimmten Vorgehensmodells bekannt. Welches Vorgehensmodell ist dies? <button onclick="toggleAnswer('tdd3')">Antwort</button>

  <span class="hidden-answer" id="tdd3">
  Testgetriebene Entwicklung wurde v.a. durch Kent Beck bekannt, der es im Rahmen des Vorgehensmodells _extreme programming_ (XP) entwickelt hat.
  </span>


4) In der Softwareentwicklung verwenden wir bei der Problemlösung häufig die Strategien Atomisieren (Aufteilen in kleinere, lösbare Probleme) und Abstrahieren (Ähnlichkeiten zu bekannten Problemlösungen finden). Beschreibe auf welche Art Testgetriebene Entwicklung eine dieser Strategien nutzt. <button onclick="toggleAnswer('tdd4')">Antwort</button>

  <span class="hidden-answer" id="tdd4">
  Testgetriebene Entwicklung geht davon aus, dass jeweils nur kleine Abschnitte der Spezifikation getestet und entwickelt werden. Durch diese Kleinschrittigkeit ist der Fokus auf ein Teilproblem gesetzt. Die anderen bereits realisierten Abschnitte geraten durch die dafür bereits realisierten Tests jedoch nicht in Vergessenheit, sondern werden von der Testsuite weiterhin abgedeckt. Das Lösen von kleinen Teilen der Spezifikation als zentraler Bestandteil von TDD steht für die _Atomisierung_ von Problemstellungen.
  </span>

5) Zur Testfallerstellung für Software können Whitebox- und Blackbox-Verfahren genutzt werden. Beschreibe, welches dieser beiden Verfahren bei der Testgetriebenen Entwicklung eingesetzt wird - und warum das jeweils andere nicht genutzt wird. <button onclick="toggleAnswer('tdd5')">Antwort</button>

  <span class="hidden-answer" id="tdd5">
  Bei der Testgetriebenen Entwicklung entsteht der Test vor der eigentlichen Implementierung. Daher können diese Tests nur gegen die Spezifikation erstellt werden, also aus Blackboxsicht. Whitebox-Tests könnten im Nachgang aufgrund der Abdeckungsmetriken ergänzt werden, das ist jedoch nicht (mehr) Bestandteil des TDD-Zyklus.
  </span>

6) Für ein Projekt, an dem Ihr Betrieb seit einem halben Jahr arbeitet, wurden für den letzten Sprint das Sprintziel "Code mit Unit-Tests abdecken" festgelegt. Die meisten Module besitzen bislang nur wenige Integrations- und keinerlei Unittests. Nach Abschluss des Sprints konnte das Team tatsächlich eine Codeüberdeckung von 100% erreichen. Wurde das Projekt nach dieser Phase testgetrieben entwickelt?  Falls nein: Welche Änderungen wären erforderlich, damit es als "testgetrieben entwickelt" bezeichnet werden kann? <button onclick="toggleAnswer('tdd6')">Antwort</button>

  <span class="hidden-answer" id="tdd6">
  Der Ablauf, Tests erst während oder nach der Implementierung zu erstellen widerspricht den Grundsätzen der Testgetriebenen Entwicklung. Testgetrieben wäre das Vorgehen nur dann, wenn für kleine Spezifikationsabschnitte zunächst ein Test, danach der zugehörige Code implementiert würde, gemäß TDD-Zyklus.
  </span>

7) Ab welchem Überdeckungsgrad darf man von "testgetriebener Entwicklung" sprechen?<button onclick="toggleAnswer('tdd7')">Antwort</button>

  <span class="hidden-answer" id="tdd7">
  Der Überdeckungsgrad hat keinerlei bezug zur Testgetriebenen Entwicklung. Daher gibt es auch keine Festlegung, welche Testabdeckung erreicht werden soll. Die Überdeckungsmetriken können genutzt werden, um nach der Erstellung von Blackboxtests und nach der darauf folgenden Implementierung Hinweise darauf zu erhalten, welche Testfälle man ggf. noch ergänzen sollte.
  </span>

8) Bei testgetriebener Entwicklung wird in Phasen vorgegangen. Benenne alle Phasen, in denen der eigentliche Programmcode angepasst wird! <button onclick="toggleAnswer('tdd8')">Antwort</button>

  <span class="hidden-answer" id="tdd8">
  In den Phasen _Green_ und _Refactor_ wird der Programmcode bearbeitet, wobei nur in _Green_ neue Funktionalitäten hinzugefügt werden.
  </span>

9) Benenne alle Phasen, in denen der Testcode angepasst wird! <button onclick="toggleAnswer('tdd9')">Antwort</button>

  <span class="hidden-answer" id="tdd9">
  In den Phasen _Red_ und _Refactor_ wird der Testcode bearbeitet, wobei nur in _Red_ neue Tests für Funktionalitäten entstehen.
  </span>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
