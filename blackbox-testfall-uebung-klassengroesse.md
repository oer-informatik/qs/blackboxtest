## Übungsaufgabe "Klassengröße" zu Blackbox-Testfallerstellung

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-klassengroesse</span>


> **tl/dr;** _(ca. 8 min Bearbeitungszeit): Kleine Übungsaufgaben zur Erstellung von Testfällen nach Blackboxsystematik für eine Methode, die die Anzahl der einzurichtenden Klassen berechnen soll._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)

Die Methode `int einzurichtendeKlassen(long anzahlSchueler; long schuelerProKlasseMax)` berechnet die Anzahl der Klassen, die bei einer Schülerzahl eingerichtet werden muss – unter Berücksichtigung einer Obergrenze an Schülern pro Klasse. Die Methode soll systematisch getestet werden. Welche Testfälle wären möglich? Bereite eine fachgerechte tabellarische Testfalldokumentation vor und trage darin vier Testfälle (zwei je Blackbox-Systematik) ein mit allen Daten, die bereits bekannt sind (die anderen Zellen können vorbereitet, aber leer bleiben).Nenne auch die zugrunde gelegte Systematik. <button onclick="toggleAnswer('bb6')">Antwort</button>

<div class="hidden-answer" id="bb6">

|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte<br/> (Parameter `anzahlSchueler`, `schuelerProKlasseMax` ) | Erwartetes<br/>Resultat | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|1. | GW: Anzahl Schüler geht auf | keine |`anzahlSchueler=60`<br/>, `schuelerProKlasseMax=30`| 2 | - |??| ??|
|2. | GW: 0 Schüler | keine |`anzahlSchueler=0`<br/>, `schuelerProKlasseMax=30`| 0 | - |??| ??|
|2. | ÄK: Anzahl Schüler geht nicht auf | keine |`anzahlSchueler=61`<br/>, `schuelerProKlasseMax=30`| 3 | - |??| ??|
|2. | ÄK: Weniger Schüler als max | keine |`anzahlSchueler=28`<br/>, `schuelerProKlasseMax=30`| 1 | - |??| ??|
</div>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
