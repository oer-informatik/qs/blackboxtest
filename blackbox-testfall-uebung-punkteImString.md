## Blackboxtest-Übungsaufgabe "Punkte im String"

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523089116938885</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/blackbox-testfall-uebung-punkteImString</span>


> **tl/dr;** _(ca. 8 min Bearbeitungszeit): Übungsaufgaben zur Erstellung von Testfällen nach Blackboxsystematik für eine Methode, die Punkte in einem String zählt._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)


Die Methode `int anzahlDerPunkteImString(String text)` soll systematisch getestet werden. Welche Systematiken legst Du an? Welche Testfälle wären möglich? Bereite eine fachgerechte tabellarische Testfalldokumentation vor und trage darin vier Testfälle (zwei je Blackbox-Systematik) ein mit allen Daten, die bereits bekannt sind (die anderen Zellen können vorbereitet, aber leer bleiben). Nenne auch die zugrunde gelegte Systematik. <button onclick="toggleAnswer('bb5')">Antwort</button>

<div class="hidden-answer" id="bb5">
Da keine Implementierung vorliegt, aber die Spezifikation im Wesentlichen aus dem Namen hervorgeht, bietet sich die Blackboxsystematik (Äquivalenzklassen, Grenzwerte) an: <br/>
<br/>

|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte<br/> (Parameter `text`) | Erwartetes<br/>Resultat | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|1. | ÄK: mehrere Punkte | keine |`anzahlDerPunkteImString("1.2.3")`| 2 | - |??| ??|
|2. | ÄK: kein Punkt | keine |`anzahlDerPunkteImString("nix")`| 0 | - |??| ??|
|3. | GW: Leerer String | keine |anzahlDerPunkteImString("")| 0 | - |??| ??|
|4. | GW: Ungültige Werte | keine |anzahlDerPunkteImString(NULL)| Exception | - |??| ??|
</div>

### Weiter Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Blackbox-Tests](https://oer-informatik.de/blackboxtests)
